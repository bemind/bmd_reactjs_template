import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { ApolloLink } from 'apollo-link';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { setContext } from 'apollo-link-context';
import { onError } from 'apollo-link-error';

const httpLink = new HttpLink({ uri: process.env.REACT_APP_GRAPHQL_URL });

// create a request interceptor to append token on auth
const authLink = setContext((req, { headers }) => {
  const token = localStorage.getItem('token');
  return {
    ...headers,
    headers: {
      authorization: token || null,
    },
  };
});

const errorLink = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors) {
    // eslint-disable-next-line array-callback-return
    graphQLErrors.map(({ message, locations, path }) => {
      const msg = `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`;
      console.log(msg);
      // TODO:message bar
    });
  }
  if (networkError) {
    const msg = `[Network error]: ${networkError}`;
    console.log(msg);
    // TODO: message bar
  }
});

const link = ApolloLink.from([
  authLink,
  errorLink,
  httpLink,
]);

const memoryCache = new InMemoryCache({
  dataIdFromObject: object => `${object.typename}-${object.id}`,
});

export default () => new ApolloClient({
  link,
  cache: memoryCache,
});
