import React, { Component } from 'react';
// Router
import {
  BrowserRouter,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
// Constants
import STATUS from '../Helper/Status';
// Containers
import Home from './Home';
import Login from './Login';
import Profile from './Profile';
import Signup from './Signup';
import CompleteSignup from './CompleteSignup';
import CheckCode from './CheckCode';
import VerifyUser from './VerifyUser';

class RootContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: STATUS.INITIAL,
      token: localStorage.getItem('token'),
    };

    this.authenticate = this.authenticate.bind(this);
    this.logout = this.logout.bind(this);
  }

  componentDidMount() {
    const { token } = this.state;
    if (token != null) {
      this.setState({ status: STATUS.TOKEN });
    }
  }

  authenticate(status, token) {
    localStorage.setItem('token', token);
    this.setState({
      status,
      token,
    });
  }

  logout() {
    localStorage.clear();
    this.setState({
      status: STATUS.INITIAL,
      token: '',
    });
  }

  render() {
    const { status, token } = this.state;

    if (status === STATUS.TOKEN) {
      return <VerifyUser authenticate={this.authenticate} logout={this.logout} token={token} />;
    }
    if (status === STATUS.AUTHENTICATED) {
      return (
        <BrowserRouter>
          <Switch>
            <Redirect from="/home" to="/" />
            <Redirect from="/complete" to="/" />
            <Route path="/" exact component={Home} />
            <Route path="/profile" exact component={Profile} />
          </Switch>
        </BrowserRouter>
      );
    }
    return (
      <BrowserRouter>
        <Switch>
          <Redirect from="/login" to="/" />
          <Route path="/signup" exact component={Signup} />
          <Route
            path="/complete"
            exact
            render={props => (
              <CompleteSignup {...props} authenticate={this.authenticate} />
            )}
          />
          <Route path="/check" exact component={CheckCode} />
          <Route
            path="/"
            exact
            render={() => (
              <Login authenticate={this.authenticate} />
            )}
          />
        </Switch>
      </BrowserRouter>
    );
  }
}


export default RootContainer;
