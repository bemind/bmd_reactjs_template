import React, { Component } from 'react';
// GraphQL
import { compose, withApollo } from 'react-apollo';
// Query
import VerifyToken from './Queries/VerifyToken.graphql';
// Mutation
import RefreshToken from './Mutations/RefreshToken.graphql';
// Constants
import STATUS from '../Helper/Status';

class VerifyUser extends Component {
  componentDidMount() {
    const { client: { query, mutate } } = this.props;
    const { authenticate, logout, token } = this.props;

    query({
      query: VerifyToken,
      variables: {
        token,
      },
    }).then((response) => {
      if (response.data.VerifyToken) {
        mutate({
          mutation: RefreshToken,
          variables: {
            token,
          },
        }).then((mutationResponse) => {
          authenticate(STATUS.AUTHENTICATED, mutationResponse.data.RefreshToken.token);
        }).catch(() => {
          // TODO: Message Bar
        });
      } else {
        logout();
      }
    }).catch(() => {
      logout();
    });
  }

  render() {
    return <h2>Loading...</h2>;
  }
}

export default compose(
  withApollo,
)(VerifyUser);
