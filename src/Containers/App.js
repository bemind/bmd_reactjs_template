// React
import React from 'react';
// Apollo
import { ApolloProvider } from 'react-apollo';
import createClient from '../Apollo';
// Containers
import RootContainer from './RootContainer';

const client = createClient();

const App = () => (
  <ApolloProvider client={client}>
    <RootContainer />
  </ApolloProvider>
);

export default App;
