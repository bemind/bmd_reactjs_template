import React, { Component } from 'react';
// Graphql
import {
  graphql,
  compose,
  withApollo,
} from 'react-apollo';
// Mutation
import CompleteUser from './Mutations/CompleteUser.graphql';
import CreateToken from './Mutations/CreateToken.graphql';
// Query
import AreasQuery from './Queries/Areas.graphql';
import AvatarsQuery from './Queries/Avatars.graphql';
// Constants
import STATUS from '../Helper/Status';

class CompleteSignup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      cpf: '',
      profession: '',
      phone: '',
      url: '',
      yearBirth: '',
      hourlyValue: '',
      description: '',
      areaIds: [],
    };
  }

  handleSubmit = (e) => {
    e.preventDefault();

    const {
      completeUser,
      createToken,
      location: { state: { email, password, code } },
    } = this.props;

    const {
      name,
      cpf,
      profession,
      phone,
      url,
      yearBirth,
      hourlyValue,
      description,
      avatarId,
      areaIds,
    } = this.state;

    const input = {
      name,
      cpf,
      profession,
      phone,
      url,
      yearBirth,
      hourlyValue,
      description,
      avatarId,
      areaIds,
    };

    completeUser({
      variables: {
        email,
        code,
        input,
      },
    }).then((response) => {
      if (response.data.CompleteUser && response.data.CompleteUser.id) {
        createToken({
          variables: {
            email,
            password,
          },
        }).then((data) => {
          const { authenticate } = this.props;
          authenticate(STATUS.TOKEN, data.data.CreateToken.token);
        }).catch(() => {
          // TODO: message bar
        });
      }
    }).catch(() => {
      // TODO: message bat
    });
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleNumber = (e) => {
    this.setState({ [e.target.name]: parseInt(e.target.value, 10) });
  }

  handleCheckbox = (e) => {
    const { areaIds } = this.state;
    const check = e.target.checked;
    const checkedArea = e.target.value;
    if (check) {
      this.setState({
        areaIds: [...areaIds, checkedArea],
      });
    } else {
      const index = areaIds.indexOf(checkedArea);
      if (index > -1) {
        areaIds.splice(index, 1);
        this.setState({
          areaIds,
        });
      }
    }
  }

  render() {
    const {
      name,
      cpf,
      profession,
      phone,
      url,
      yearBirth,
      hourlyValue,
      description,
    } = this.state;

    const {
      areas,
      avatars,
      areas: { Areas },
      avatars: { Avatars },
    } = this.props;

    if (areas.loading || avatars.loading) {
      return (
        <h2>Loading...</h2>
      );
    }

    const areaInputs = Areas.map(area => (
      <li key={area.id}>
        <div style={{ backgroundColor: area.color }}>
          <input type="checkbox" name={area.name} value={area.id} onChange={this.handleCheckbox} />
          {area.name}
        </div>
      </li>
    ));

    const avatarInputs = Avatars.map(avatar => (
      <li key={avatar.id}>
        <input type="radio" name="avatarId" value={avatar.id} onChange={this.handleChange} />
        <img alt={avatar.name} src={avatar.icon} />
      </li>
    ));

    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <input type="text" name="name" placeholder="name" value={name} onChange={this.handleChange} />
          <input type="text" name="cpf" placeholder="cpf" value={cpf} onChange={this.handleChange} />
          <input type="text" name="profession" placeholder="profession" value={profession} onChange={this.handleChange} />
          <input type="text" name="phone" placeholder="phone" value={phone} onChange={this.handleChange} />
          <input type="text" name="url" placeholder="url" value={url} onChange={this.handleChange} />
          <input type="number" name="yearBirth" placeholder="yearBirth" value={yearBirth} onChange={this.handleNumber} />
          <input type="number" name="hourlyValue" placeholder="hourlyValue" value={hourlyValue} onChange={this.handleNumber} />
          <input type="text" name="description" placeholder="description" value={description} onChange={this.handleChange} />
          <ul>
            {areaInputs}
          </ul>
          <ul>
            {avatarInputs}
          </ul>
          <input type="submit" value="Submit" />
        </form>
      </div>
    );
  }
}

const completeSignup = graphql(CompleteUser, {
  name: 'completeUser',
});
const createToken = graphql(CreateToken, {
  name: 'createToken',
});
const areas = graphql(AreasQuery, {
  name: 'areas',
});
const avatars = graphql(AvatarsQuery, {
  name: 'avatars',
});

export default compose(
  completeSignup,
  createToken,
  areas,
  avatars,
  withApollo,
)(CompleteSignup);
