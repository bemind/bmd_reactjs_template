import React, { Component } from 'react';
// Route
import { Switch, Redirect } from 'react-router-dom';
// GraphQL
import { compose, withApollo } from 'react-apollo';
// Query
import CheckPasswordCode from './Queries/CheckPasswordCode.graphql';

class CheckCode extends Component {
  constructor(props) {
    super(props);

    this.state = {
      code: '',
      email: '',
      password: '',
      checked: false,
    };
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { code } = this.state;
    const { client: { query }, location: { state: { email, password } } } = this.props;

    query({
      query: CheckPasswordCode,
      variables: {
        email,
        code,
      },
    }).then((response) => {
      if (response.data.CheckPasswordCode) {
        this.setState({
          email,
          password,
          checked: true,
        });
      }
    }).catch(() => {
      // TODO: messagebar
    });
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  render() {
    const {
      email,
      password,
      code,
      checked,
    } = this.state;
    if (checked) {
      return (
        <Switch>
          <Redirect to={{
            pathname: '/complete',
            state: {
              email,
              password,
              code,
            },
          }}
          />
        </Switch>
      );
    }
    return (
      <div>
        <h2>
          Check your code
          <br />
        </h2>
        <form onSubmit={this.handleSubmit}>
          <input type="number" name="code" value={code} placeholder="Code" onChange={this.handleChange} />
          <input type="submit" value="submit" />
        </form>
      </div>
    );
  }
}

export default compose(
  withApollo,
)(CheckCode);
