// React
import React, { Component } from 'react';
import { toast } from 'react-toastify';
// GraphQL
import { graphql, compose, withApollo } from 'react-apollo';
// Route
import { Link } from 'react-router-dom';
// Query
import UserQuery from './Queries/User.graphql';

class Home extends Component {
  handleLogout = () => {
    localStorage.clear();
    window.location.replace('/');
  }

  notify = () => toast('Alesson');

  render() {
    const { data } = this.props;
    if (data.loading) {
      return (
        <h2>Loading...</h2>
      );
    }

    const { User } = data;
    return (
      <div>
        <h2>
          {`Hello, ${User.name}`}
        </h2>
        <button type="button" onClick={this.handleLogout}>Logout</button>
        <Link to="/profile">Profile</Link>
      </div>
    );
  }
}

const user = graphql(UserQuery);

export default compose(
  user,
  withApollo,
)(Home);
