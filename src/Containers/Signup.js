import React, {
  Component,
} from 'react';
import {
  Formik,
  Form,
  Field,
  ErrorMessage,
} from 'formik';
// Route
import { Switch, Redirect } from 'react-router-dom';
// graphql
import {
  graphql,
  compose,
  withApollo,
} from 'react-apollo';
import { toast, ToastContainer } from 'react-toastify';
// Mutation
import CreateUser from './Mutations/CreateUser.graphql';
// Helper
import ValidationSchema from '../Helper/ValidationSchema';

class Signup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      next: false,
    };
  }

  notify = (text) => {
    toast.error(text);
  };

  handleSubmit = (actions) => {
    const {
      email,
      password,
    } = this.state;

    console.log(this.state);

    const { mutate } = this.props;

    mutate({
      variables: {
        input: {
          email,
          password,
        },
      },
    }).then((response) => {
      console.log(response);
      if (response.data.CreateUser) {
        this.setState({ next: true });
      }
      actions.setSubmitting(false);
    }).catch((error) => {
      this.notify(error.message);
      actions.setSubmitting(false);
    });
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
    console.log(this.state);
  }

  render() {
    const { email, password, next } = this.state;
    if (next) {
      return (
        <Switch>
          <Redirect to={{
            pathname: '/check',
            state: {
              email,
              password,
            },
          }}
          />
        </Switch>
      );
    }
    return (
      <div>
        <Formik
          initialValues={{ email: '', password: '' }}
          onSubmit={this.handleSubmit}
          validationSchema={ValidationSchema}
        >
          {({ isSubmitting }) => (
            <Form>
              <Field type="email" name="email" onChange={this.handleChange} />
              <ErrorMessage name="email" component="div" />
              <Field type="password" name="password" onChange={this.handleChange} />
              <ErrorMessage name="password" component="div" />
              <button type="submit" disabled={isSubmitting}>
                Continue
              </button>
              <ToastContainer />
            </Form>
          )}

        </Formik>
      </div>
    );
  }
}

const signup = graphql(CreateUser);

export default compose(
  signup,
  withApollo,
)(Signup);
