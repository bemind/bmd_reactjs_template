import React, {
  Component,
} from 'react';
import {
  Formik,
  Form,
  Field,
  ErrorMessage,
} from 'formik';
// Route
import { Link } from 'react-router-dom';
// graphql
import { toast, ToastContainer } from 'react-toastify';
import {
  graphql,
  compose,
  withApollo,
} from 'react-apollo';
// Mutation
import CreateToken from './Mutations/CreateToken.graphql';
// Constants
import STATUS from '../Helper/Status';
// Helper
import LoginSchema from '../Helper/ValidationSchema';

class Login extends Component {
  notify = (text) => {
    toast.error(text);
  };

  handleLogin = (values, actions) => {
    const {
      email,
      password,
    } = values;

    const { mutate } = this.props;

    mutate({
      variables: {
        email,
        password,
      },
    }).then((response) => {
      const { authenticate } = this.props;
      authenticate(STATUS.TOKEN, response.data.CreateToken.token);
      actions.setSubmitting(false);
    }).catch(() => {
      this.notify('Invalid email or password');
      actions.setSubmitting(false);
    });
  }

  render() {
    return (
      <div>
        <h1>Login</h1>
        <Formik
          initialValues={{ email: '', password: '' }}
          validationSchema={LoginSchema}
          onSubmit={this.handleLogin}
        >
          {({ isSubmitting }) => (
            <Form>
              <Field type="email" name="email" />
              <ErrorMessage name="email" component="div" />
              <Field type="password" name="password" />
              <ErrorMessage name="password" component="div" />
              <button type="submit" disabled={isSubmitting}>
                Login
              </button>
              <Link to="/signup">Signup</Link>
              <ToastContainer />
            </Form>
          )}
        </Formik>
      </div>
    );
  }
}

const login = graphql(CreateToken);

export default compose(
  login,
  withApollo,
)(Login);
