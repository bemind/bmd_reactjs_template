import React from 'react';
import {
  graphql,
  compose,
  withApollo,
} from 'react-apollo';
import UserQuery from './Queries/User.graphql';

const Profile = (props) => {
  const { data } = props;

  if (data.loading) {
    return (<h2>Loading...</h2>);
  }
  const { User } = data;

  const areas = User.areas.map(area => (
    <li key={area.id}>
      <div style={{ backgroundColor: area.color }}>
        {area.name}
      </div>
    </li>
  ));

  return (
    <div>
      <img alt="Avatar" src={User.avatar.icon} />
      <h2>
        {`Name: ${User.name}`}
      </h2>
      <h2>
        {`Email: ${User.email}`}
      </h2>
      <h2>
        {`Profession: ${User.profession}`}
      </h2>
      <h2>
        {`CPF: ${User.cpf}`}
      </h2>
      <h2>
        {`Phone: ${User.phone}`}
      </h2>
      <h2>
        {`Birth Year: ${User.yearBirth}`}
      </h2>
      <h2>
        {`Hourly Value: ${User.hourlyValue}`}
      </h2>
      <h2>
        {`Description: ${User.description}`}
      </h2>
      <ul>
        {areas}
      </ul>
    </div>
  );
};

const profile = graphql(UserQuery);

export default compose(
  profile,
  withApollo,
)(Profile);
