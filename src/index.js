// react
import React from 'react';
import ReactDOM from 'react-dom';
// Container
import App from './Containers/App';
// css
import './index.css';

ReactDOM.render(<App />, document.getElementById('app'));

module.hot.accept();
