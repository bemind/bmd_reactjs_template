const STATUS = {
  INITIAL: 'initial',
  LOADING: 'loading',
  TOKEN: 'token',
  AUTHENTICATED: 'authenticated',
};

export default STATUS;
